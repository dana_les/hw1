
struct myStack
{
	int maxSize;
	int count;
	int element;
	bool empty;
	struct myStack *next;
};

void push(struct myStack *s, int element);  // insert element to top of the stack
int pop(struct myStack *s); //remove element from top of the stack

void initStack(struct myStack *s, int maxSize, int ele, int count);
void cleanStack(struct myStack *s);

bool isEmpty(struct myStack *s);
bool isFull(struct myStack *s);

#include "Stack.h"


void push(struct myStack *s, int element)
{
	struct myStack* currNode = s;
	struct myStack* new_struct = new struct myStack;
	if (s)
	{
		while (currNode->next)
		{
			currNode = currNode->next;
		}
		initStack(new_struct, currNode->maxSize, element, currNode->count + 1);
		currNode->next = new_struct;
	}
	else
	{
		s = new_struct;
		new_struct->next = 0;
	}
}

int pop(struct myStack *s)
{
	struct myStack* currNode = s;
	struct myStack* currNode2 = s;
	int ret = -1;
	if (currNode)
	{
		if (!(currNode->next))
		{
			ret = currNode->element;
			delete (s);
		}
		else
		{
			currNode = currNode->next;
			while (currNode->next)
			{
				currNode2 = currNode2->next;
				currNode = currNode->next;
			}
			ret = currNode->element;
			delete (currNode);
			currNode2->next = 0;
		}
	}

	return(ret);
}

void initStack(struct myStack *s, int maxSize, int ele, int count)
{
	s->element = ele;
	s->count = count;
	s->maxSize = maxSize;
	s->next = 0;
}

void cleanStack(myStack *s)
{
	while (s != nullptr);
}

bool isEmpty(myStack *s)
{
	return(s->empty);
}

bool isFull(myStack *s)
{
	if (!isEmpty(s))
	{
		while (s->next)
		{
			s = s->next;
		}
	}
	return(s->count == s->maxSize);
}
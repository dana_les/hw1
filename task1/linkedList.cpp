#include "linkedList.h"

int delete_from_list(struct linked_list** first)
{
	struct linked_list* currNode = *first;
	struct linked_list* currNode2 = *first;
	int ret = -1;
	if (currNode)
	{
		if (!(currNode->next))
		{
			ret = currNode->num;
			delete (*first);
		}
		else
		{
			currNode = currNode->next;
			while (currNode->next)
			{
				currNode2 = currNode2->next;
				currNode = currNode->next;
			}
			ret = currNode->num;
			delete (currNode);
			currNode2->next = 0;
		}
	}

	return(ret);
}

void add_to_list(struct linked_list** first, int number)
{
	struct linked_list* currNode = *first;
	struct linked_list* new_struct = new struct linked_list;
	new_struct->num = number;
	new_struct->next = 0;
	if (*first)
	{
		while (currNode->next)
		{
			currNode = currNode->next;
		}
		currNode->next = new_struct;
	}
	else
	{
		*first = new_struct;
		new_struct->next = 0;
	}
}
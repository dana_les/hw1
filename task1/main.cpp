#include <iostream>
#include "Stack.h"

void reverse(int* nums, int size);

int main()
{
	int numbers[10];
	int i;
	for (i = 0; i < 10; i++)
	{
		std::cin >> numbers[i];
	}
	reverse(numbers, 10);
	for (i = 0; i < 10; i++)
	{
		std::cout << numbers[i] << std::endl;
	}
	system("pause");
	return 0;
}

void reverse(int* nums, int size)
{
	struct myStack* numbers = new struct myStack;
	int i;
	initStack(numbers, size, 0, 0);
	for (i = 0; i < size; i++)
	{
		push(numbers, nums[i]);
	}
	for (i = 0; i < size; i++)
	{
		nums[i] = pop(numbers);
	}
}
#include <iostream>
#include "myQueue.h"

int pop(struct myQueue* Queue)
{
	int ret = -1;
	if (Queue->change_stack)
	{
		if (!isEmpty(Queue))
		{
			Queue->stack1->count--;
			ret = Queue->stack1->elements[Queue->stack1->count];
			Queue->stack1->empty = (Queue->stack1->count == 0);
		}
	}
	else 
	{
		if (!isEmpty(Queue))
		{
			Queue->stack2->count--;
			ret = Queue->stack2->elements[Queue->stack2->count];
			Queue->stack2->empty = (Queue->stack2->count == 0);
		}
	}
	return ret;
}



int dequeue(struct myQueue* Queue)
{
	int element, ret = -1;
	Queue->change_stack = false;
	initStack(Queue, Queue->stack1->maxSize);
	Queue->change_stack = true;
	while (!isEmpty(Queue))
	{
		Queue->change_stack = true;
		element = pop(Queue);
		Queue->change_stack = false;
		enqueue(Queue, element);
		Queue->change_stack = true;
	}
	Queue->change_stack = false;
	ret = pop(Queue);
	Queue->change_stack = true;
	initStack(Queue, Queue->stack1->maxSize);
	Queue->change_stack = false;
	while (!isEmpty(Queue))
	{
		Queue->change_stack = false;
		element = pop(Queue);
		Queue->change_stack = true;
		enqueue(Queue, element);
		Queue->change_stack = false;
	}
	return(ret);
}

void enqueue(struct myQueue* Queue, int element)
{
	if (Queue->change_stack)
	{
		if (!isFull(Queue))
		{
			Queue->stack1->elements[Queue->stack1->count] = element;
			Queue->stack1->count++;
			Queue->stack1->empty = false;
		}
	}
	else
	{
		if (!isFull(Queue))
		{
			Queue->stack2->elements[Queue->stack2->count] = element;
			Queue->stack2->count++;
			Queue->stack2->empty = false;
		}
	}
}

void initStack(struct myQueue *Queue, int size)
{
	struct myStack* stack = new struct myStack;
	stack->count = 0;
	stack->empty = true;
	stack->maxSize = size;
	stack->elements = new int[size];
	if (Queue->change_stack)
	{
		Queue->stack1 = stack;
	}
	else
	{
		Queue->stack2 = stack;
	}
}
void cleanStack(struct myQueue *Queue)
{
	delete[] Queue->stack1->elements;
	delete[] Queue->stack2->elements;
}

bool isEmpty(struct myQueue *Queue)
{
	bool ret;
	if (Queue->change_stack)
	{
		ret =  Queue->stack1->empty;
	}
	else
	{
		ret = Queue->stack2->empty;
	}
	return(ret);
}
bool isFull(struct myQueue *Queue)
{
	bool ret;
	if (Queue->change_stack)
	{
		ret = Queue->stack1->count == Queue->stack1->maxSize;
	}
	else
	{
		ret = Queue->stack2->count == Queue->stack2->maxSize;
	}
	return(ret);
}



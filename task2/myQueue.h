struct myQueue
{
	struct myStack* stack1;
	struct myStack* stack2;
	bool change_stack;	//if false - stack2 need to be changes else stack 1
};

struct myStack
{
	int maxSize;
	int count;
	bool empty;
	int *elements;
};



int pop(struct myQueue* Queue);

int dequeue(struct myQueue* Queue);  // remove element from buttom of the queue

void enqueue(struct myQueue* Queue, int number);  // insert element to top of the queue

void initStack(struct myQueue *Queue, int size);
void cleanStack(struct myQueue *Queue);

bool isEmpty(struct myQueue *Queue);
bool isFull(struct myQueue *Queue);

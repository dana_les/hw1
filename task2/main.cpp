#include <iostream>
#include "myQueue.h"

int main()
{
	struct myQueue* Queue = new struct myQueue;
	Queue->change_stack = true;
	initStack(Queue, 3);
	enqueue(Queue, 4);
	enqueue(Queue, 5);
	enqueue(Queue, 6);
	Queue->change_stack = true;
	while (!isEmpty(Queue))
	{
		std::cout << dequeue(Queue) << std::endl;
		Queue->change_stack = true;
	}
	system("pause");
	return 0;
}
#include "mergeSort.h"
#include <iostream>

const int ARR_LENGTH = 10;

int main()
{
	int numbers[ARR_LENGTH] = { 1, 8, 5, 7, 4, 9, 6, 3, 0, 2 };
	int i;
	int numbersArr[ARR_LENGTH];
	sort_arr(numbers, numbersArr);

	for (i = 0; i < 10; i++)
	{
		std::cout << numbers[i] << std::endl;
	}
	system("pause");
	return 0;
}
#include "mergeSort.h"

const int ARR_LENGTH = 10;

void sort_arr(int arr[], int temp[])
{
	mergeSort(arr, temp, 0, ARR_LENGTH - 1);
}

void merge(int arr[], int temp[], int left, int mid, int right)
{
	int leftPlace = left, rightPlace = mid + 1;
	bool leftOver = false, rightOver = false;	


	for (int place = left; place <= right; place++)
	{
		if (leftPlace > mid)
		{
			leftOver = true;
		}
		if (rightPlace > right)
		{
			rightOver = true;
		}
		//
		if (rightOver || (!leftOver && arr[leftPlace] <= arr[rightPlace]))
		{
			temp[place] = arr[leftPlace];
			leftPlace++;
		}
		else
		{
			temp[place] = arr[rightPlace];
			rightPlace++;
		}
	}
	for (int i = left; i <= right; i++)
	{
		arr[i] = temp[i];
	}
}

void mergeSort(int arr[], int temp[], int left, int right)
{
	int mid = (right + left) / 2;

	if (mid > left)		//left side
	{
		mergeSort(arr, temp, left, mid);
	}


	if (right > mid + 1)		//right side
	{
		mergeSort(arr, temp, mid + 1, right);
	}

	merge(arr, temp, left, mid, right);	// uniting both sides
}
